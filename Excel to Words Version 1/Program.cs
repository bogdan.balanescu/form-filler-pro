﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Excel_to_Words_Version_1
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            /*if (checkIfInstalled() == false)
            {
                return;
            }*/
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        public static bool checkIfInstalled()
        {
            string installPath = (string)Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\bdaniel54", "installed", null);
            if (installPath == null)
            {
                MessageBox.Show("This application is not properly installed! Please use the installation kit!");
                return false;
            }
            else if (!installPath.Equals("installed"))
            {
                MessageBox.Show("It appears that some files have been corrupt! Please reinstall/repair using the installation kit!");
                return false;
            }
            else
            {
                //MessageBox.Show("Good to go!");
                return true;
            }
        }
    }
}
