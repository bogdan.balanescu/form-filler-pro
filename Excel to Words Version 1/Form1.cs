﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Excel_to_Words_Version_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (checkIfLicensed() == false)
            {
                DisableControls();
            }
            else
            {
                EnableControls();
            }

            // LISTVIEW1 initialization
            listView1.View = View.Details;
            listView1.GridLines = true;
            listView1.FullRowSelect = true;

            //Add column header
            listView1.Columns.Add("Name", 150);
            listView1.Columns.Add("Date", 50);
            listView1.Columns.Add("Type", 42);
            listView1.Columns.Add("Size", 42);

            //listView1.Items.Add(new ListViewItem(new[] { "0", "1", "2", "3", "4"}));
        }

        private void EnableControls()
        {
            licenseTextBox.Visible = false;
            activateButton.Visible = false;
            startButton.Enabled = true;
            folderButton.Enabled = true;
            wordButton.Enabled = true;
            excelButton.Enabled = true;
            singleWordRadioButton.Enabled = true;
            multiWordRadioButton.Enabled = true;
            fieldCharacterTextBox.Enabled = true;
            sheetNumberComboBox.Enabled = true;
            folderTextBox.Enabled = true;
            wordTextBox.Enabled = true;
            excelTextBox.Enabled = true;
            listView1.Enabled = true;
        }

        private void DisableControls()
        {
            licenseTextBox.Visible = true;
            activateButton.Visible = true;
            startButton.Enabled = false;
            folderButton.Enabled = false;
            wordButton.Enabled = false;
            excelButton.Enabled = false;
            singleWordRadioButton.Enabled = false;
            multiWordRadioButton.Enabled = false;
            fieldCharacterTextBox.Enabled = false;
            sheetNumberComboBox.Enabled = false;
            folderTextBox.Enabled = false;
            wordTextBox.Enabled = false;
            excelTextBox.Enabled = false;
            listView1.Enabled = false;
        }

        private static bool checkIfLicensed()
        {
            string[] licenses = getLicenseKeys();
            string licensePath = (string)Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\bdaniel54", "license", null);
            //licensePath = "WAVGX-8DRJB-8WVS4-QN601-XOEAK-GHF6U";
            if (licensePath == null)
            {
                MessageBox.Show("Your application is not activated! Please insert a lincese and press \"Activate\"!");
                return false;
            }
            else if (!(Array.IndexOf(licenses, licensePath) >= 0))
            {
                MessageBox.Show("Your application is not activated! Please insert a lincese and press \"Activate\"!");
                return false;
            }
            else
            {
                //MessageBox.Show("Good to go!");
                return true;
            }
        }

        private static string[] getLicenseKeys()
        {
            string[] licenses = { "WAVGX-8DRJB-8WVS4-QN601-XOEAK-GHF6U",
                                  "D2Y30-TH9Z4-160LW-617AE-KR02Y-1RSEY",
                                  "GMGT3-XVN4A-TODY3-W0AHQ-OAG3T-ATE77",
                                  "MFOB4-HIWGW-5S2QQ-9Q622-3OE7X-8H7LB",
                                  "1Z0VH-B2OQC-WD4ZR-DIPY2-R5W7J-A6VM3",
                                  "X6YNM-GCG55-7JDG2-MI8AG-GBZL3-J2N57",
                                  "OWJ6D-0O0RE-D0IH9-KK9K1-IZ0P2-MKDX1",
                                  "LKRLC-BQWJM-6TDRX-TCO43-WX0M6-RPQX3",
                                  "1M5ZY-WT3IA-FAXD4-KN5AY-PUYT9-URS8P",
                                  "AKWY1-77F1M-3VR32-90A2U-LI3WU-YZISY",
                                  "02W87-GNALG-CNO5M-8R8EI-XN3C4-G6P7F",
                                  "DD0AN-GRSOB-FGEDT-3UMPN-G0DVR-0GX0D",
                                  "W1EH3-ICL70-Z4QUT-76ATY-0JSLY-9BHU2",
                                  "UI50M-XML3V-99VEC-W8AYT-C1MYJ-BVB8X",
                                  "QIZMJ-WCQ4I-E2HQW-8O637-9N3IO-6U4CZ",
                                  "UNNSO-4B9UG-MBVU9-NZQW5-5T6JX-YEAI5",
                                  "AR0P2-23P34-GGYPU-ZUSI4-HGGLN-SHP7U",
                                  "CM478-FOA8T-W0TAK-OEWPR-H2AHG-LYJGW",
                                  "FODUU-JA6RS-6UWKM-MRFKX-M2TMM-OHTW9",
                                  "8L0QK-BV06W-116IC-01SBN-CILOG-WC7WF"};
            return licenses;
        }

        private void activateButton_Click(object sender, EventArgs e)
        {
            string[] licenses = getLicenseKeys();
            if (Array.IndexOf(licenses, licenseTextBox.Text) >= 0)
            {
                Registry.SetValue(@"HKEY_CURRENT_USER\SOFTWARE\bdaniel54", "license", licenseTextBox.Text);
                MessageBox.Show("Your application is now activated!");
                EnableControls();
            }
            else
            {
                licenseTextBox.Text = "";
                MessageBox.Show("Could not activate! Invalid license!");
            }
        }

        private void wordButton_Click(object sender, EventArgs e)
        {
            wordOpenFileDialog.Title = "Deschide Word";
            wordOpenFileDialog.Filter = "Fisiere Word (*.doc;*.docx)|*.doc;*.docx";
            wordOpenFileDialog.FileName = "";
            wordOpenFileDialog.InitialDirectory = "MyDocuments";
            wordOpenFileDialog.CheckFileExists = true;
            if (wordOpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                wordTextBox.Text = wordOpenFileDialog.FileName;
            }
        }

        private void excelButton_Click(object sender, EventArgs e)
        {
            excelOpenFileDialog.Title = "Deschide Excel";
            excelOpenFileDialog.Filter = "Fisiere Excel (*.xlsx;*.xls;*.xla)|*.xlsx;*.xls;*.xla";
            excelOpenFileDialog.FileName = "";
            excelOpenFileDialog.InitialDirectory = "MyDocuments";
            excelOpenFileDialog.CheckFileExists = true;
            if (excelOpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                excelTextBox.Text = excelOpenFileDialog.FileName;
                excelTextBox_Leave(null, null);
            }
        }

        private void folderButton_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.RootFolder = System.Environment.SpecialFolder.Desktop;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                folderTextBox.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            if ((wordTextBox.Text.Contains(".doc") || wordTextBox.Text.Contains(".docx"))
                && (excelTextBox.Text.Contains(".xlsx") || excelTextBox.Text.Contains(".xls") || excelTextBox.Text.Contains(".xla"))
                && folderTextBox.Text != ""
                && sheetNumberComboBox.Text != "" 
                && fieldCharacterTextBox.Text != ""
                && (singleWordRadioButton.Checked || multiWordRadioButton.Checked))
            {
                try
                {
                    ExcelHandler excelHandler = new ExcelHandler();
                    WordHandler wordHandler = new WordHandler();

                    //1. Open Excel
                    //2. Open Word
                    excelHandler.OpenExcel(excelTextBox.Text, sheetNumberComboBox.Text);
                    wordHandler.OpenWord(wordTextBox.Text);

                    //3. Check Compatibility
                    progressLabel.Text = "Progress: checking compatibility..."; // PROGRESS LABEL
                    string pattern = fieldCharacterTextBox.Text + fieldCharacterTextBox.Text + "+";
                    int number = wordHandler.NumberOfFields(pattern);
                    if (number < excelHandler.Range.Columns.Count)
                    {
                        progressLabel.Text = "Incompatible files (excel has too many columns)";
                        MessageBox.Show("Incompatible files (excel has too many columns)");
                        wordHandler.CloseWord();
                        excelHandler.CloseExcel();
                        return; // END
                    }
                    else if (number > excelHandler.Range.Columns.Count)
                    {
                        progressLabel.Text = "Incompatible files (excel has too few columns)";
                        MessageBox.Show("Incompatible files (excel has too few columns)");
                        wordHandler.CloseWord();
                        excelHandler.CloseExcel();
                        return; // END
                    }

                    //4. Close Word
                    wordHandler.CloseWord();

                    //5. Writing document/documents
                    progressLabel.Text = "Progress: Writing documents...";
                    progressBar1.Maximum = excelHandler.Range.Rows.Count; // PROGRESS BAR
                    progressBar1.Value = 0; // PROGRESS BAR
                    progressBar1.Step = 1; // PROGRESS BAR
                    progressBar1.Enabled = true; // PROGRESS BAR
                    if (singleWordRadioButton.Checked)
                    {
                        // SINGLE WORD ROUTINE
                        // multiply word content in destination word
                        string wordName = Path.GetFileName(wordTextBox.Text);
                        string currentWord;
                        if (wordTextBox.Text.Contains(".docx"))
                        {
                            currentWord = folderTextBox.Text + "/" + wordName.Replace(".docx", "-result" + ".docx");
                        }
                        else
                        {
                            currentWord = folderTextBox.Text + "/" + wordName.Replace(".doc", "-result" + ".doc");
                        }
                        wordHandler.SingleMultiplication(wordTextBox.Text, currentWord, excelHandler.Range.Rows.Count);

                        for (int i = 1; i <= excelHandler.Range.Rows.Count; i++)
                        {
                            // read excel row content
                            string[] rowContent = excelHandler.ReadRow(i);

                            // replace content in word
                            wordHandler.ReplaceContentInWord(currentWord, rowContent, pattern);

                            progressBar1.PerformStep();
                            if (progressBar1.Value == progressBar1.Maximum)
                            {
                                //progressBar.Value = 0;
                                progressBar1.Enabled = false;
                            }
                        }
                        listView1.Items.Add(new ListViewItem(new[] { Path.GetFileName(currentWord).ToString(), File.GetCreationTime(currentWord).ToString(), Path.GetExtension(currentWord).ToString(), new FileInfo(currentWord).Length.ToString(), currentWord }));
                    }
                    else if (multiWordRadioButton.Checked)
                    {
                        // MULTI WORD ROUTINE
                        for (int i = 1; i <= excelHandler.Range.Rows.Count; i++)
                        {
                            // read excel row content
                            string[] rowContent = excelHandler.ReadRow(i);
                            // duplicate word
                            string wordName = Path.GetFileName(wordTextBox.Text);
                            string currentWord;
                            if (wordTextBox.Text.Contains(".docx"))
                            {
                                currentWord = folderTextBox.Text + "/" + wordName.Replace(".docx", i.ToString() + ".docx");
                            }
                            else
                            {
                                currentWord = folderTextBox.Text + "/" + wordName.Replace(".doc", i.ToString() + ".doc");
                            }
                            wordHandler.DuplicateWord(wordTextBox.Text, currentWord);
                            // replace content in word
                            wordHandler.ReplaceContentInWord(currentWord, rowContent, pattern);

                            listView1.Items.Add(new ListViewItem(new[] { Path.GetFileName(currentWord).ToString(), File.GetCreationTime(currentWord).ToString(), Path.GetExtension(currentWord).ToString(), new FileInfo(currentWord).Length.ToString(), currentWord }));
                            progressBar1.PerformStep();
                            if (progressBar1.Value == progressBar1.Maximum)
                            {
                                //progressBar.Value = 0;
                                progressBar1.Enabled = false;
                            }
                        }
                    }

                    //6. Close Excel
                    excelHandler.CloseExcel();

                    //7. Write complete message
                    progressLabel.Text = "Progress: documents successfully written!";
                    MessageBox.Show("Action complete!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error occured " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Invalid input data!" + "\n"
                    + "Make sure the word file, the excel file and the folder path are valid!" + "\n"
                    + "Check if the sheet number and the field character are not empty!" + "\n"
                    + "Also check one of the single word or multi word buttons!");
            }
        }

        private void fieldCharacterTextBox_Leave(object sender, EventArgs e)
        {
            if (fieldCharacterTextBox.Text.Length > 1)
            {
                MessageBox.Show("Only one character can be provided for the field!");
                fieldCharacterTextBox.Text = "";
                return;
            }
            if ("\\^${}[]().*+?|<>-&".Contains(fieldCharacterTextBox.Text))
            {
                MessageBox.Show("The field character cannot be any of these: " +
                    "\\^${}[]().*+?|<>-&." + 
                    "\nPlease try another character!");
                fieldCharacterTextBox.Text = "";
                return;
            }
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(listView1.SelectedItems[0].SubItems[4].Text);
        }

        private void openWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            wordButton.PerformClick();
        }

        private void openExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            excelButton.PerformClick();
        }

        private void openFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            folderButton.PerformClick();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void minimizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void maximizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void normalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackColor = SystemColors.Control;
            this.ForeColor = SystemColors.ControlText;
            listView1.BackColor = SystemColors.Window;
            listView1.ForeColor = SystemColors.WindowText;
            groupBox1.BackColor = SystemColors.Control;
            groupBox1.ForeColor = SystemColors.ControlText;
            //wordButton.BackColor = SystemColors.Control;
            //wordButton.ForeColor = SystemColors.ControlText;
            //excelButton.BackColor = SystemColors.Control;
            //excelButton.ForeColor = SystemColors.ControlText;
            //folderButton.BackColor = SystemColors.Control;
            //folderButton.ForeColor = SystemColors.ControlText;
            //startButton.BackColor = SystemColors.Control;
            //startButton.ForeColor = SystemColors.ControlText;
        }

        private void skyBlueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.ForeColor = System.Drawing.Color.DarkBlue;
            listView1.BackColor = System.Drawing.Color.LightBlue;
            listView1.ForeColor = System.Drawing.Color.DarkBlue;
            groupBox1.BackColor = System.Drawing.Color.SkyBlue;
            groupBox1.ForeColor = System.Drawing.Color.DarkBlue;
            //wordButton.BackColor = System.Drawing.Color.SkyBlue;
            //wordButton.ForeColor = System.Drawing.Color.DarkBlue;
            //excelButton.BackColor = System.Drawing.Color.SkyBlue;
            //excelButton.ForeColor = System.Drawing.Color.DarkBlue;
            //folderButton.BackColor = System.Drawing.Color.SkyBlue;
            //folderButton.ForeColor = System.Drawing.Color.DarkBlue;
            //startButton.BackColor = System.Drawing.Color.SkyBlue;
            //startButton.ForeColor = System.Drawing.Color.DarkBlue;
        }

        private void darkThemeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.Color.DimGray;
            this.ForeColor = System.Drawing.Color.DarkBlue;
            listView1.BackColor = System.Drawing.Color.DimGray;
            listView1.ForeColor = System.Drawing.Color.DarkBlue;
            groupBox1.BackColor = System.Drawing.Color.DimGray;
            groupBox1.ForeColor = System.Drawing.Color.DarkBlue;
        }

        private void howTouseTheProgramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Hi! This program automatically completes data from"
                + "an excel file into a word file in the place of the fields given!\n"
                + "Usage tips:\n"
                + "1. Use the 'Load Word' button to choose the template word file.\n"
                + "2. Use the 'Load Excel' button to choose the excel file which has the data you need.\n"
                + "3. Use the 'Select result folder' button to choose the path of the results.\n"
                + "4. In the 'Excel Sheet Name' box, write/select the name of the sheet that you want to use from the excel file.\n"
                + "5. In the 'Field Character' box, write a the type of the character that you want to be replaced in the word file. "
                + "The program will replace sequences of at least two characters like the one give. For example: if the character give is '_' the program will replace sequences like '__' or like '___' or like '____' etc.\n"
                + "6. Select the type of the result you want: 'Single Word' or 'Multi Word'.\n"
                + "7. Press the 'Start' button and wait.\n");
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This program is made by Balanescu Bogdan-Daniel\n"
                + "Contact: bogdan.balanescu@yahoo.com");
        }

        private void excelTextBox_Leave(object sender, EventArgs e)
        {
            sheetNumberComboBox.Items.Clear();
            sheetNumberComboBox.Text = "";
            ExcelHandler excelHandler = new ExcelHandler();
            //1. Open Excel and get the sheets list
            List<String> list = excelHandler.OpenExcelGetSheetsList(excelTextBox.Text);
            if (list == null)
            {
                return;
            }
            //2. Add the sheets list to the combo box
            foreach (String s in list)
            {
                sheetNumberComboBox.Items.Add(s);
            }
            if (sheetNumberComboBox.Items.Count > 0)
            {
                sheetNumberComboBox.Text = sheetNumberComboBox.Items[0].ToString();
            }
        }
    }
}
