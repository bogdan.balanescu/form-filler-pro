﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace Excel_to_Words_Version_1
{
    class ExcelHandler
    {
        private Excel.Application xlApp;
        private Excel.Workbook xlWorkBook;
        private Excel.Worksheet xlWorkSheet;
        private Excel.Range range;
        object misValue = System.Reflection.Missing.Value;

        public ExcelHandler()
        {
        }

        public Excel.Application XLApp { get { return this.xlApp; } set { this.xlApp = value; } }
        public Excel.Workbook XMLWorkBook { get { return this.xlWorkBook; } set { this.xlWorkBook = value; } }
        public Excel.Worksheet XMLWorkSheet { get { return this.xlWorkSheet; } set { this.xlWorkSheet = value; } }
        public Excel.Range Range { get { return this.range; } set { this.range = value; } }

        public void OpenExcel(string excelName, string sheetNumber)
        {
            try
            {
                xlApp = new Excel.Application();
                xlWorkBook = xlApp.Workbooks.Open(excelName);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(sheetNumber);

                range = xlWorkSheet.UsedRange;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Error at: open excel");
                throw ex;
            }
        }

        public string[] ReadRow(int rowCount)
        {
            try
            {
                string[] result = new string[range.Columns.Count];
                for (int columnCount = 1; columnCount <= range.Columns.Count; columnCount++)
                {
                    if ((range.Cells[rowCount, columnCount] as Excel.Range).Value2 != null)
                    {
                        string str = ((range.Cells[rowCount, columnCount] as Excel.Range).Value2).ToString();
                        result[columnCount - 1] = str;
                    }
                    else
                    {
                        result[columnCount - 1] = "";
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Error at: read row");
                throw ex;
            }
        }

        public void CloseExcel()
        {
            try
            {
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();

                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Error at: close excel");
                throw ex;
            }
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                System.Windows.Forms.MessageBox.Show("Unable to release the Object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        internal List<string> OpenExcelGetSheetsList(string excelName)
        {
            try
            {
                Excel.Application xlAppLocal;
                xlAppLocal = new Excel.Application();
                xlAppLocal.Workbooks.Open(excelName);
                List<String> sheetsList = new List<string>();
                foreach (Excel.Worksheet displayWorksheet in xlAppLocal.Worksheets)
                {
                    sheetsList.Add(displayWorksheet.Name);
                }
                try
                {
                    xlAppLocal.Quit();

                    releaseObject(xlAppLocal);
                }
                catch (Exception)
                {
                    //System.Windows.Forms.MessageBox.Show("Error at: close excel (GETTING SHEETS NAMES)");
                    //throw ex;
                }
                return sheetsList;
            }
            catch (Exception)
            {
                //System.Windows.Forms.MessageBox.Show("Error at: open excel (GETTING SHEETS NAMES)");
                //throw ex;
            }
            return null;
        }
    }
}
