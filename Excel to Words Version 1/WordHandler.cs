﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;
using System.IO;
using System.Text.RegularExpressions;
using System.Reflection;
using Microsoft.Office.Interop.Word;

namespace Excel_to_Words_Version_1
{
    class WordHandler
    {
        private WordprocessingDocument wordDoc;

        public WordHandler()
        {
        }

        public WordprocessingDocument WordDoc { get { return this.wordDoc; } set { this.wordDoc = value; } }

        public void OpenWord(string wordName)
        {
            try
            {
                wordDoc = WordprocessingDocument.Open(wordName, true);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Error at: open word");
                throw ex;
            }
        }

        public int NumberOfFields(string fieldPattern)
        {
            try
            {
                string documentText;
                using (StreamReader reader = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                {
                    documentText = reader.ReadToEnd();
                    reader.Close();
                }
                Match m = Regex.Match(documentText, fieldPattern);
                int mCount = 0;
                while (m.ToString() != "")
                {
                    mCount++;
                    m = m.NextMatch();
                }
                return mCount;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Error at: number of fields");
                throw ex;
            }
        }

        public void DuplicateWord(string oldDoc, string newDoc)
        {
            try
            {
                using (var mainDoc = WordprocessingDocument.Open(oldDoc, false))
                using (var resultDoc = WordprocessingDocument.Create(newDoc, WordprocessingDocumentType.Document))
                {
                    // copy parts from source document to new document
                    foreach (var part in mainDoc.Parts)
                        resultDoc.AddPart(part.OpenXmlPart, part.RelationshipId);
                    // perform replacements in resultDoc.MainDocumentPart
                    // ...
                    mainDoc.Close();
                    resultDoc.Close();
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Error at: duplicate word");
                throw ex;
            }
        }

        public void ReplaceContentInWord(string docPath, string[] newContent, string fieldPattern)
        {
            try
            {
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(docPath, true))
                {
                    string documentText;
                    using (StreamReader reader = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                    {
                        documentText = reader.ReadToEnd();
                        reader.Close();
                    }
                    Match m = Regex.Match(documentText, fieldPattern);
                    for (int cCnt = 0; cCnt < newContent.Length; cCnt++)
                    {
                        var regex = new Regex(Regex.Escape(m.Value));
                        documentText = regex.Replace(documentText, newContent[cCnt], 1);
                        m = m.NextMatch();
                        using (StreamWriter writer = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                        {
                            writer.Write(documentText);
                            writer.Flush();
                            writer.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Error at: replace content in word");
                throw ex;
            }
        }

        public void SingleMultiplication(string mainWord, string currentWord, int count)
        {
            Object missing = Missing.Value;
            ApplicationClass wordApp = new ApplicationClass();
            wordApp.DisplayAlerts = WdAlertLevel.wdAlertsAll;
            Microsoft.Office.Interop.Word.Document document = wordApp.Documents.Open(
                mainWord, ref missing, ref missing, ref missing, ref missing,
                ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, false, true);
            document.Activate();
            document.ActiveWindow.Selection.WholeStory();
            document.ActiveWindow.Selection.Copy();
            document.Close(false);

            document = wordApp.Documents.Add(ref missing, ref missing, ref missing, true);
            document.Activate();
            wordApp.Visible = true;
            for (int i = 1; i < count; i++)
            {
                document.ActiveWindow.Selection.PasteAndFormat(WdRecoveryType.wdFormatOriginalFormatting);
                document.ActiveWindow.Selection.InsertBreak(WdBreakType.wdPageBreak);
            }
            document.ActiveWindow.Selection.PasteAndFormat(WdRecoveryType.wdFormatOriginalFormatting);
            document.SaveAs2(currentWord, ref missing, ref missing, ref missing, ref missing,
                ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, false, true);
            document.Close();
            wordApp.Quit(false, false, false);
        }

        public void CloseWord()
        {
            try
            {
                wordDoc.Close();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Error at: close word");
                throw ex;
            }
        }
    }
}
