﻿namespace Excel_to_Words_Version_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listView1 = new System.Windows.Forms.ListView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openWordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.minimizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maximizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.designToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.skyBlueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.darkThemeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.howTouseTheProgramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folderTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.excelTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.wordTextBox = new System.Windows.Forms.TextBox();
            this.folderButton = new System.Windows.Forms.Button();
            this.startButton = new System.Windows.Forms.Button();
            this.excelButton = new System.Windows.Forms.Button();
            this.wordButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.wordOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.excelOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.progressLabel = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.multiWordRadioButton = new System.Windows.Forms.RadioButton();
            this.singleWordRadioButton = new System.Windows.Forms.RadioButton();
            this.fieldCharacterTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.sheetNumberComboBox = new System.Windows.Forms.ComboBox();
            this.activateButton = new System.Windows.Forms.Button();
            this.licenseTextBox = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(12, 27);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(285, 231);
            this.listView1.TabIndex = 0;
            this.listView1.TabStop = false;
            this.toolTip1.SetToolTip(this.listView1, "Results document list");
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.DoubleClick += new System.EventHandler(this.listView1_DoubleClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.windowToolStripMenuItem,
            this.designToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(714, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openWordToolStripMenuItem,
            this.openExcelToolStripMenuItem,
            this.openFolderToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openWordToolStripMenuItem
            // 
            this.openWordToolStripMenuItem.Name = "openWordToolStripMenuItem";
            this.openWordToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.openWordToolStripMenuItem.Text = "Open &Word";
            this.openWordToolStripMenuItem.Click += new System.EventHandler(this.openWordToolStripMenuItem_Click);
            // 
            // openExcelToolStripMenuItem
            // 
            this.openExcelToolStripMenuItem.Name = "openExcelToolStripMenuItem";
            this.openExcelToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.openExcelToolStripMenuItem.Text = "Open &Excel";
            this.openExcelToolStripMenuItem.Click += new System.EventHandler(this.openExcelToolStripMenuItem_Click);
            // 
            // openFolderToolStripMenuItem
            // 
            this.openFolderToolStripMenuItem.Name = "openFolderToolStripMenuItem";
            this.openFolderToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.openFolderToolStripMenuItem.Text = "Open &Folder";
            this.openFolderToolStripMenuItem.Click += new System.EventHandler(this.openFolderToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // windowToolStripMenuItem
            // 
            this.windowToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.minimizeToolStripMenuItem,
            this.maximizeToolStripMenuItem});
            this.windowToolStripMenuItem.Name = "windowToolStripMenuItem";
            this.windowToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.windowToolStripMenuItem.Text = "&Window";
            // 
            // minimizeToolStripMenuItem
            // 
            this.minimizeToolStripMenuItem.Name = "minimizeToolStripMenuItem";
            this.minimizeToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.minimizeToolStripMenuItem.Text = "Mi&nimize";
            this.minimizeToolStripMenuItem.Click += new System.EventHandler(this.minimizeToolStripMenuItem_Click);
            // 
            // maximizeToolStripMenuItem
            // 
            this.maximizeToolStripMenuItem.Name = "maximizeToolStripMenuItem";
            this.maximizeToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.maximizeToolStripMenuItem.Text = "Maxi&mize";
            this.maximizeToolStripMenuItem.Click += new System.EventHandler(this.maximizeToolStripMenuItem_Click);
            // 
            // designToolStripMenuItem
            // 
            this.designToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.normalToolStripMenuItem,
            this.skyBlueToolStripMenuItem,
            this.darkThemeToolStripMenuItem});
            this.designToolStripMenuItem.Name = "designToolStripMenuItem";
            this.designToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.designToolStripMenuItem.Text = "&Design";
            // 
            // normalToolStripMenuItem
            // 
            this.normalToolStripMenuItem.Name = "normalToolStripMenuItem";
            this.normalToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.normalToolStripMenuItem.Text = "&Normal theme";
            this.normalToolStripMenuItem.Click += new System.EventHandler(this.normalToolStripMenuItem_Click);
            // 
            // skyBlueToolStripMenuItem
            // 
            this.skyBlueToolStripMenuItem.Name = "skyBlueToolStripMenuItem";
            this.skyBlueToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.skyBlueToolStripMenuItem.Text = "&Sky theme";
            this.skyBlueToolStripMenuItem.Click += new System.EventHandler(this.skyBlueToolStripMenuItem_Click);
            // 
            // darkThemeToolStripMenuItem
            // 
            this.darkThemeToolStripMenuItem.Name = "darkThemeToolStripMenuItem";
            this.darkThemeToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.darkThemeToolStripMenuItem.Text = "&Dark theme";
            this.darkThemeToolStripMenuItem.Click += new System.EventHandler(this.darkThemeToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.howTouseTheProgramToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // howTouseTheProgramToolStripMenuItem
            // 
            this.howTouseTheProgramToolStripMenuItem.Name = "howTouseTheProgramToolStripMenuItem";
            this.howTouseTheProgramToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.howTouseTheProgramToolStripMenuItem.Text = "How to &use the program";
            this.howTouseTheProgramToolStripMenuItem.Click += new System.EventHandler(this.howTouseTheProgramToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // folderTextBox
            // 
            this.folderTextBox.Location = new System.Drawing.Point(311, 153);
            this.folderTextBox.Name = "folderTextBox";
            this.folderTextBox.Size = new System.Drawing.Size(390, 20);
            this.folderTextBox.TabIndex = 5;
            this.toolTip1.SetToolTip(this.folderTextBox, "Results folder path");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(308, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 16);
            this.label4.TabIndex = 17;
            this.label4.Text = "Result folder:";
            // 
            // excelTextBox
            // 
            this.excelTextBox.Location = new System.Drawing.Point(311, 99);
            this.excelTextBox.Name = "excelTextBox";
            this.excelTextBox.Size = new System.Drawing.Size(390, 20);
            this.excelTextBox.TabIndex = 4;
            this.toolTip1.SetToolTip(this.excelTextBox, "Excel source path");
            this.excelTextBox.Leave += new System.EventHandler(this.excelTextBox_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(311, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 16);
            this.label3.TabIndex = 15;
            this.label3.Text = "Excel source:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(312, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 14;
            this.label2.Text = "Word source:";
            // 
            // wordTextBox
            // 
            this.wordTextBox.Location = new System.Drawing.Point(311, 46);
            this.wordTextBox.Name = "wordTextBox";
            this.wordTextBox.Size = new System.Drawing.Size(390, 20);
            this.wordTextBox.TabIndex = 3;
            this.toolTip1.SetToolTip(this.wordTextBox, "Word source path");
            // 
            // folderButton
            // 
            this.folderButton.Location = new System.Drawing.Point(540, 255);
            this.folderButton.Name = "folderButton";
            this.folderButton.Size = new System.Drawing.Size(161, 23);
            this.folderButton.TabIndex = 12;
            this.folderButton.Text = "Select result folder";
            this.toolTip1.SetToolTip(this.folderButton, "Select results folder");
            this.folderButton.UseVisualStyleBackColor = true;
            this.folderButton.Click += new System.EventHandler(this.folderButton_Click);
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(540, 315);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(161, 23);
            this.startButton.TabIndex = 13;
            this.startButton.Text = "Start";
            this.toolTip1.SetToolTip(this.startButton, "Start");
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // excelButton
            // 
            this.excelButton.Location = new System.Drawing.Point(540, 226);
            this.excelButton.Name = "excelButton";
            this.excelButton.Size = new System.Drawing.Size(161, 23);
            this.excelButton.TabIndex = 11;
            this.excelButton.Text = "Load Excel";
            this.toolTip1.SetToolTip(this.excelButton, "Load excel");
            this.excelButton.UseVisualStyleBackColor = true;
            this.excelButton.Click += new System.EventHandler(this.excelButton_Click);
            // 
            // wordButton
            // 
            this.wordButton.Location = new System.Drawing.Point(540, 197);
            this.wordButton.Name = "wordButton";
            this.wordButton.Size = new System.Drawing.Size(161, 23);
            this.wordButton.TabIndex = 10;
            this.wordButton.Text = "Load Word";
            this.toolTip1.SetToolTip(this.wordButton, "Load word");
            this.wordButton.UseVisualStyleBackColor = true;
            this.wordButton.Click += new System.EventHandler(this.wordButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.label1.Location = new System.Drawing.Point(308, 186);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 16);
            this.label1.TabIndex = 23;
            this.label1.Text = "Excel Sheet Name:";
            // 
            // wordOpenFileDialog
            // 
            this.wordOpenFileDialog.FileName = "openFileDialog1";
            // 
            // excelOpenFileDialog
            // 
            this.excelOpenFileDialog.FileName = "openFileDialog2";
            // 
            // progressLabel
            // 
            this.progressLabel.AutoSize = true;
            this.progressLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.progressLabel.Location = new System.Drawing.Point(12, 287);
            this.progressLabel.Name = "progressLabel";
            this.progressLabel.Size = new System.Drawing.Size(66, 16);
            this.progressLabel.TabIndex = 25;
            this.progressLabel.Text = "Progress:";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 315);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(285, 23);
            this.progressBar1.TabIndex = 26;
            this.toolTip1.SetToolTip(this.progressBar1, "Progress bar");
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.multiWordRadioButton);
            this.groupBox1.Controls.Add(this.singleWordRadioButton);
            this.groupBox1.Location = new System.Drawing.Point(311, 292);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(215, 46);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Choose:";
            // 
            // multiWordRadioButton
            // 
            this.multiWordRadioButton.AutoSize = true;
            this.multiWordRadioButton.Location = new System.Drawing.Point(133, 19);
            this.multiWordRadioButton.Name = "multiWordRadioButton";
            this.multiWordRadioButton.Size = new System.Drawing.Size(76, 17);
            this.multiWordRadioButton.TabIndex = 9;
            this.multiWordRadioButton.Text = "Multi Word";
            this.toolTip1.SetToolTip(this.multiWordRadioButton, "Multiple word result");
            this.multiWordRadioButton.UseVisualStyleBackColor = true;
            // 
            // singleWordRadioButton
            // 
            this.singleWordRadioButton.AutoSize = true;
            this.singleWordRadioButton.Location = new System.Drawing.Point(6, 19);
            this.singleWordRadioButton.Name = "singleWordRadioButton";
            this.singleWordRadioButton.Size = new System.Drawing.Size(83, 17);
            this.singleWordRadioButton.TabIndex = 8;
            this.singleWordRadioButton.Text = "Single Word";
            this.toolTip1.SetToolTip(this.singleWordRadioButton, "Single word result");
            this.singleWordRadioButton.UseVisualStyleBackColor = true;
            // 
            // fieldCharacterTextBox
            // 
            this.fieldCharacterTextBox.Location = new System.Drawing.Point(311, 262);
            this.fieldCharacterTextBox.Name = "fieldCharacterTextBox";
            this.fieldCharacterTextBox.Size = new System.Drawing.Size(215, 20);
            this.fieldCharacterTextBox.TabIndex = 7;
            this.toolTip1.SetToolTip(this.fieldCharacterTextBox, "Field character");
            this.fieldCharacterTextBox.Leave += new System.EventHandler(this.fieldCharacterTextBox_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.label6.Location = new System.Drawing.Point(308, 242);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 16);
            this.label6.TabIndex = 28;
            this.label6.Text = "Field Character:";
            // 
            // sheetNumberComboBox
            // 
            this.sheetNumberComboBox.FormattingEnabled = true;
            this.sheetNumberComboBox.Location = new System.Drawing.Point(309, 205);
            this.sheetNumberComboBox.Name = "sheetNumberComboBox";
            this.sheetNumberComboBox.Size = new System.Drawing.Size(217, 21);
            this.sheetNumberComboBox.TabIndex = 6;
            // 
            // activateButton
            // 
            this.activateButton.Location = new System.Drawing.Point(626, 2);
            this.activateButton.Name = "activateButton";
            this.activateButton.Size = new System.Drawing.Size(75, 23);
            this.activateButton.TabIndex = 2;
            this.activateButton.Text = "Activate";
            this.activateButton.UseVisualStyleBackColor = true;
            this.activateButton.Click += new System.EventHandler(this.activateButton_Click);
            // 
            // licenseTextBox
            // 
            this.licenseTextBox.Location = new System.Drawing.Point(311, 4);
            this.licenseTextBox.Name = "licenseTextBox";
            this.licenseTextBox.Size = new System.Drawing.Size(309, 20);
            this.licenseTextBox.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 359);
            this.Controls.Add(this.licenseTextBox);
            this.Controls.Add(this.activateButton);
            this.Controls.Add(this.sheetNumberComboBox);
            this.Controls.Add(this.fieldCharacterTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.progressLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.folderButton);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.excelButton);
            this.Controls.Add(this.wordButton);
            this.Controls.Add(this.folderTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.excelTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.wordTextBox);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Excel to Words Version 1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openWordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem minimizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maximizeToolStripMenuItem;
        private System.Windows.Forms.TextBox folderTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox excelTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox wordTextBox;
        private System.Windows.Forms.Button folderButton;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button excelButton;
        private System.Windows.Forms.Button wordButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.OpenFileDialog wordOpenFileDialog;
        private System.Windows.Forms.OpenFileDialog excelOpenFileDialog;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label progressLabel;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton multiWordRadioButton;
        private System.Windows.Forms.RadioButton singleWordRadioButton;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem howTouseTheProgramToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.TextBox fieldCharacterTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolStripMenuItem designToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem skyBlueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem darkThemeToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ComboBox sheetNumberComboBox;
        private System.Windows.Forms.Button activateButton;
        private System.Windows.Forms.TextBox licenseTextBox;
    }
}

